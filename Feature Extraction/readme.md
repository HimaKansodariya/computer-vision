# feature Extraction Task

Finding similar object from two images using opencv only

### Understanding and Execusion 

Here I have used to algorithm to find object

* (1) Brute-Force Matching with SIFT 

* (2) FLANN based Matcher

#### Brute-Force Matching with SIFT

* use distance calculation
* returns closest one 
* need two input. Query Image and Train Image

#### Flann based matching 

* contains a collection of algorithms 
* fast nearest neighbor search in large datasets
* for high dimensional features
* need two input. Query Image and Train Image



