# task

* Linear Filter (blur, sharpen, edge detect and contour)
+ Median Filter, Bilateral Filter, Morphology
* Image Blending (crop one image in circle shape and put inside another)
* Template matching