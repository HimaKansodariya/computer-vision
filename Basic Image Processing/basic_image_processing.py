import cv2
import numpy as np
from matplotlib import pyplot as plt

flower = cv2.imread('.\\flower.jpg')
flower_template = cv2.imread('.\\flower_template.jpg',0)
noisy_img = cv2.imread('.\\noisy_img.jpg')
dog = cv2.imread('.\\dog.jpg',1)
grass = cv2.imread('.\\grass.jpg',1)


def show_image(name, image):
    cv2.imshow(name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def linear_gaussian_blur(image):
    # Gaussian Blur
    show_image('original image',image)
    gaussian = cv2.GaussianBlur(image, (7, 7), 0)
    show_image('Linear Gaussian Blurring', gaussian)


def linear_sharpen(image):
    # Creating our sharpening filter
    sharpen_filter = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    # Applying cv2.filter2D function on our image
    sharpen_img = cv2.filter2D(image, -1, sharpen_filter)
    show_image('Sharped Image', sharpen_img)


def linear_edge(image):
    edges = cv2.Canny(image, 100, 200)
    plt.subplot(121), plt.imshow(image, cmap='gray')
    plt.title('Original Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(edges, cmap='gray')
    plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
    plt.show()


def contour(image):
    imgray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contoured_image = cv2.drawContours(image, contours, -1, (0, 255 ,0), 3)
    show_image('contoured image', contoured_image)


def median_filter(image):
    rows, columns, *z = image.shape
    # Traverse the image. For every 3X3 area,find the median of the pixels and replace the center pixel by the median
    img_new1 = np.zeros([rows, columns])
    for i in range(1, rows - 1):
        for j in range(1, columns - 1):
            temp = [image[i - 1, j - 1][0],
                    image[i - 1, j][0],
                    image[i - 1, j + 1][0],
                    image[i, j - 1][0],
                    image[i, j][0],
                    image[i, j + 1][0],
                    image[i + 1, j - 1][0],
                    image[i + 1, j][0],
                    image[i + 1, j + 1][0]]
            temp = sorted(temp)
            img_new1[i, j] = temp[4] # median
    img_new1 = img_new1.astype(np.uint8)
    show_image('median filtered', img_new1)


def bilateral_filter(image):
    bilateral = cv2.bilateralFilter(image, 15, 75, 75)
    show_image('Bilateral filtered',bilateral)


def morphology(image):
    kernel = np.ones((4, 4), np.uint8)
    erosion = cv2.erode(image, kernel, iterations=1)
    show_image('erosion', erosion)
    cv2.waitKey()
    opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
    show_image('opening', opening)


def circle_crop(image):
    # Get image size
    height, width = image.shape[:2]
    height = int(height)
    width = int(width)
    circleIn = np.zeros((height, width, 1), np.uint8)
    circleIn = cv2.circle(circleIn, (width // 2, height // 2), min(height, width) // 2, (1), -1)
    # Generate a blank image
    imgIn = np.zeros((height, width, 4), np.uint8)
    # Copy the first 3 channels
    imgIn[:, :, 0] = np.multiply(dog[:, :, 0], circleIn[:, :, 0])
    imgIn[:, :, 1] = np.multiply(dog[:, :, 1], circleIn[:, :, 0])
    imgIn[:, :, 2] = np.multiply(dog[:, :, 2], circleIn[:, :, 0])
    circleIn[circleIn == 1] = 255
    imgIn[:, :, 3] = circleIn[:, :, 0]
    show_image('Circular cropped',imgIn)
    return imgIn


def blending(image1,image2):
    image1 = circle_crop(image1)
    image1 = cv2.resize(image1, (255, 255))
    image2 = cv2.resize(image2, (255, 255))
    blended_image = cv2.addWeighted(image2, 0.7, image1, 0.3, 0)
    show_image('blened Image', blended_image)


def template_matching(image,template):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    width, height = template.shape[::-1]
    match = cv2.matchTemplate(gray_image, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    position = np.where(match >= threshold)  # get the location of template in the image
    for point in zip(*position[::-1]):  # draw the rectangle around the matched template
        cv2.rectangle(image, point, (point[0] + width, point[1] + height), (0, 204, 153), 0)
    show_image('Template Found', image)


# template_matching(flower,flower_template)
# linear_gaussian_blur(noisy_img)
# median_filter(noisy_img)
# bilateral_filter(noisy_img)
circle_crop(flower)
contour(dog)
linear_edge(dog)


