# Task: Machine learning on Vision 

script to convert given image into n color cluster 

## Understanding and Executuion 

here, to divide a image into color cluster Kmeans clustering algorithm is being used. 

### steps to perform 

* read the given image using OpenCV
* reshape the images and conver it into numpy array
* define critarea for number of iteration and epsilon value
* fit and apply kmeans
* convert array back into image
* display/save image

### different type of image taken for clustering 

* multicolor image having same amount of color distribution
* image having maximum 3 to 4 colors 
* multicolored image having colors distribution randomly
* images having multiple sades of same color

 --> after taking different images as input, it can be said that value of K varies of different type of images. We need to find best K for given image  
 
 ### Finding best K
 
 #### method 1: Elbow of optimal value of K
 
 * determining the number of clusters in a data set. 
 * The method consists of plotting the explained variation as a function of the number of clusters
 * picking the elbow of the curve as the number of clusters to use.
 *  an "elbow" cannot always be unambiguously identified
 
#### method 2: Silhouette method

* interpretation and validation of consistency within clusters of data.
* calculates the silhouette index for each sample
* average silhouette index for each cluster and overall average silhouette index for a dataset.
