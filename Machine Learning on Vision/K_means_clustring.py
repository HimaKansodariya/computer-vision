import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


# use color_cluster function on image to replace less dominate color by more dominate color
# parameter 1: path -> path of image (eg: 'image1.jpg' )
# parameter 2: value_of_K --> number of color clusters user wants
def color_cluster(path, value_of_K):

    # read the image
    img = cv.imread(path)

    # reshape the image and convert to np.float32
    X = img.reshape((-1, 3))
    X = np.float32(X)

    # define criteria
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)

    #  number of clusters(K) which is one of the parameter of this function
    K = value_of_K

    # apply kmeans()
    ret, label, center = cv.kmeans(X, K, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)

    # convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))

    # show the image
    cv.imshow('res2', res2)
    cv.waitKey(0)
    cv.destroyAllWindows()


# use silhouette_score function to find best value of K for Kmeans cluster
# parameter: array_dataset --> it takes array of data

def silhouette_score(array_dataset):

    # store array of dataset
    X = array_dataset

    # define range to get average silhouette score for size of cluster
    no_of_clusters = range(2, 20)

    # calculate avg for given range
    for n_clusters in no_of_clusters:

        # fit and predict the value
        cluster = KMeans(n_clusters=n_clusters)
        cluster_labels = cluster.fit_predict(X)

        # The silhouette_score gives the average value for all the samples.
        silhouette_avg = silhouette_score(X, cluster_labels)

        print("For no of clusters =", n_clusters,
              " The average silhouette_score is :", silhouette_avg)


def elbow_score(array_dataset):
    distortions = []
    inertias = []
    mapping1 = {}
    mapping2 = {}
    K = range(1, 20)
    Z = array_dataset
    for k in K:
        # Building and fitting the model
        kmeanModel = KMeans(n_clusters=k).fit(Z)
        kmeanModel.fit(Z)

        distortions.append(sum(np.min(cdist(Z, kmeanModel.cluster_centers_,
                                            'euclidean'), axis=1)) / Z.shape[0])
        inertias.append(kmeanModel.inertia_)

        mapping1[k] = sum(np.min(cdist(Z, kmeanModel.cluster_centers_,
                                       'euclidean'), axis=1)) / Z.shape[0]
        mapping2[k] = kmeanModel.inertia_

    plt.plot(K, distortions, 'bx-')
    plt.xlabel('Values of K')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method using Distortion')
    plt.show()


# call function and pass path and value of K (total number of clusters)
color_cluster('image2.jpg',4)
